package br.com.syonet.poo;

public class Casa {
	private Integer Valor;
	private Integer Area;
	private Integer nComodos;
	private Integer aTerreno ;
	
	
	
	
	public Casa(int valor, int area, int nComodos, int aTerreno) {
		this.Valor = valor;
		this.Area = area;
		this.nComodos = nComodos;
		this.aTerreno = aTerreno;
	}
	public Integer getValor() {
		return Valor;
	}
	public void setValor(Integer valor) {
		Valor = valor;
	}
	public Integer getArea() {
		return Area;
	}
	public void setArea(Integer area) {
		Area = area;
	}
	public Integer getnComodos() {
		return nComodos;
	}
	public void setnComudos(Integer nComodos) {
		this.nComodos = nComodos;
	}
	public Integer getaTerreno() {
		return aTerreno;
	}
	public void setaTerreno(Integer aTerreno) {
		this.aTerreno = aTerreno;
	}
	
	
	
}
