package br.com.syonet.poo;



public class Imobilária {
	
public static void main(String[] args) {
	
	Casa casa = new Casa(0,0,1,0);
	Apartamento apartamento = new Apartamento(0,0,1,true);
	Terreno terreno = new Terreno(0,0);
	
	System.out.println("Casa:");
	System.out.println("Area do imovel: " + casa.getArea());
	System.out.println("Valor do imovel: R$" + casa.getValor());
	System.out.println("Comodos: " + casa.getnComodos());
	System.out.println("Area do Terreno: " + casa.getaTerreno());
	System.out.println("Apartamento:");
	System.out.println("Area do imovel: " + apartamento.getArea());
	System.out.println("Valor do imovel: R$" + apartamento.getValor());
	System.out.println("Comodos: " + apartamento.getnComodos());
	System.out.println("Salao de festas: " + apartamento.isSalao());
	System.out.println("Terreno:");
	System.out.println("Area do imovel: " + terreno.getArea());
	System.out.println("Valor do imovel: R$" + terreno.getValor());
	
	
}

	private Casa casa;
	private Apartamento apartamento;
	private Terreno terreno;
		
	public Casa getCasa() {
		return casa;
	}
	public void setCasa(Casa casa) {
		this.casa = casa;
	}
	public Apartamento getApartamento() {
		return apartamento;
	}
	public void setApartamento(Apartamento apartamento) {
		this.apartamento = apartamento;
	}
	public Terreno getTerreno() {
		return terreno;
	}
	public void setTerreno(Terreno terreno) {
		this.terreno = terreno;
	}
		
}
