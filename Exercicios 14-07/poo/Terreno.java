package br.com.syonet.poo;

public class Terreno {
	private Integer Valor;
	private Integer Area;
	
	public Terreno(int valor, int area) {
		this.Valor = valor;
		this.Area = area;
		}
	
	public Integer getValor() {
		return Valor;
	}
	public void setValor(Integer valor) {
		Valor = valor;
	}
	public Integer getArea() {
		return Area;
	}
	public void setArea(Integer area) {
		Area = area;
	}
		
}
