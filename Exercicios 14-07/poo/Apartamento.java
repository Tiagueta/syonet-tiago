package br.com.syonet.poo;

public class Apartamento {
	private Integer Valor;
	private Integer Area;
	private Integer nComodos;
	private boolean Salao;
	
	
	public Apartamento(int valor, int area, int nComodos, boolean salao) {
		this.Valor = valor;
		this.Area = area;
		this.nComodos = nComodos;
		this.Salao = salao;
	}
	public Integer getValor() {
		return Valor;
	}
	public void setValor(Integer valor) {
		Valor = valor;
	}
	public Integer getArea() {
		return Area;
	}
	public void setArea(Integer area) {
		Area = area;
	}
	public Integer getnComodos() {
		return nComodos;
	}
	public void setnComodos(Integer nComodos) {
		this.nComodos = nComodos;
	}
	public boolean isSalao() {
		return Salao;
	}
	public void setSalao(boolean salao) {
		Salao = salao;
	}
	
	
}
