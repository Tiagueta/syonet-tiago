package br.com.syonet.poo2;

public class Contato {
	
	private String Nome;
	private Integer Telefone;
	private String Email;
	
	public Contato(String nome, int telefone, String email) {
		this.Nome = nome;
		this.Telefone = telefone;
		this.Email = email;
	}
	
	
	public String getNome() {
		return Nome;
	}
	public void setNome(String nome) {
		Nome = nome;
	}
	public Integer getTelefone() {
		return Telefone;
	}
	public void setTelefone(Integer telefone) {
		Telefone = telefone;
	}
	public String getEmail() {
		return Email;
	}
	public void setEmail(String email) {
		Email = email;
	}
}
