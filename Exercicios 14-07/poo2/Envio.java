package br.com.syonet.poo2;

import br.com.syonet.poo2.Contato;
import br.com.syonet.poo2.Remetente;

public class Envio {	

	
public static void main(String[] args) {
	
	Contato contato = new Contato("Tiago", 999999999, "tiago.herzer@syonet.com");
	Remetente remetente = new Remetente("Teste", 888888888, "teste@syonet.com");
	
	String Mensagem = "Mensagem teste";
	
	System.out.println("SMS:");
	System.out.println(remetente.getNome() + ": " + Mensagem + " +" + contato.getTelefone() );
		
	System.out.println("");
	
	System.out.println("Email:");
	System.out.println(contato.getEmail() + " - " + contato.getNome());
	System.out.println(remetente.getEmail() + " - " + remetente.getNome());
	System.out.println(Mensagem);
		
	
	}
}