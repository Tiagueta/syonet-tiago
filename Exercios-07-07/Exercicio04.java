package br.com.syonet;

import java.util.Scanner;

public class Exercicio04 {

  public static void main(String[] args) {	  
	  Scanner n = new Scanner(System.in);
	  
	  System.out.println("Digite um número:");
	  
	  int numero = n.nextInt();
	  int aux = 0;
	  
	  for(int i = 1; i <= numero; i++) {
		  if(numero % i == 0) {			  
			  aux ++;
		  }		 
		}
	  
	  if (aux == 2) {
		  System.out.println("O número " + numero + " é primo");
	  }
	  else {
		  System.out.println("O número " + numero + " não é primo");
	  }
	  
	 }  
}