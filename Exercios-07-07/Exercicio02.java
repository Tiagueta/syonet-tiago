package br.com.syonet;

import java.util.Scanner;

public class Exercicio02 {

  public static void main(String[] args) {
	  
	  Scanner n = new Scanner(System.in);

	  System.out.println("Digite um número:");
	  int numero = n.nextInt();
	  int fim = 0;
	  
	  if(numero%2==0) {
		  System.out.println("Par");
		  for(int i = 0; i <= numero; i++) {
			  if(i%2==0) {
				  fim = fim + i;				  
			  }			  
		  }
	  }
	  else {
		  System.out.println("Impar");
		  for(int i = 0; i <= numero; i++) {
			  if(i%2==1) {
				  fim = fim + i;				  
			  }			  
		  }
	  }
	  
	  System.out.println(fim);
	  
	  n.close();
  }  
}